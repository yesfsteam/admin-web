﻿using System.Linq;
using System.Security.Claims;

namespace Admin.Web.Extensions
{
	public static class ClaimsPrincipalExtensions
	{
		public static string GetUserName(this ClaimsPrincipal user)
		{
			if (user.Identity == null)
				return null;
			
			var claimValue1 = user.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
			var claimValue = user.Claims.FirstOrDefault(x => x.Type == "Name")?.Value;
			return claimValue;
		}
	}
}
