﻿using Yes.CreditApplication.Api.Contracts.Enums;

namespace Admin.Web.Extensions
{
    public static class EnumExtensions
    {
        public static string GetCssColorClass(this CreditApplicationStatus status)
        {
            switch (status)
            {
                case CreditApplicationStatus.Draft:
                    return "table-light";
                case CreditApplicationStatus.New:
                    return "table-primary";
                case CreditApplicationStatus.InProgress:
                    return "table-warning";
                case CreditApplicationStatus.UnableToContact:
                    return "table-info";
                case CreditApplicationStatus.Processed:
                    return "table-success";
                case CreditApplicationStatus.Rejected:
                    return "table-secondary";
                case CreditApplicationStatus.ClientRejected:
                    return "table-secondary";
                case CreditApplicationStatus.NeedToCallBack:
                    return "table-info";
                case CreditApplicationStatus.UnableToContactArchive:
                    return "table-secondary";
                case CreditApplicationStatus.TechnicalProblem:
                    return "table-danger";
                default:
                    return string.Empty;
            }
        }
        
        public static string GetSvgIcon(this CreditOrganizationRequestStatus status)
        {
            switch (status)
            {
                case CreditOrganizationRequestStatus.Pending:
                    return "<svg class=\"bi bi-clock-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#007bff\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z\"/></svg>";
                case CreditOrganizationRequestStatus.Approved:
                    return "<svg class=\"bi bi-plus-circle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#28a745\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4a.5.5 0 0 0-1 0v3.5H4a.5.5 0 0 0 0 1h3.5V12a.5.5 0 0 0 1 0V8.5H12a.5.5 0 0 0 0-1H8.5V4z\"/></svg>";
                case CreditOrganizationRequestStatus.Declined:
                    return "<svg class=\"bi bi-dash-circle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#dc3545\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4 7.5a.5.5 0 0 0 0 1h8a.5.5 0 0 0 0-1H4z\"/></svg>";
                case CreditOrganizationRequestStatus.ValidationErrorCheck:
                case CreditOrganizationRequestStatus.ValidationErrorConfirm:
                    return "<svg class=\"bi bi-exclamation-triangle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#ffc107\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 5zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z\"/></svg>";
                case CreditOrganizationRequestStatus.AuthorizaionErrorCheck:
                case CreditOrganizationRequestStatus.AuthorizaionErrorConfirm:
                    return "<svg class=\"bi bi-person-circle\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#dc3545\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z\"/><path fill-rule=\"evenodd\" d=\"M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z\"/><path fill-rule=\"evenodd\" d=\"M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z\"/></svg>";
                case CreditOrganizationRequestStatus.ServerErrorCheck:
                case CreditOrganizationRequestStatus.ServerErrorConfirm:
                    return "<svg class=\"bi bi-exclamation-circle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#dc3545\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z\"/></svg>";
                case CreditOrganizationRequestStatus.Confirmed:
                    return "<svg class=\"bi bi-check-circle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#28a745\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z\"/></svg>";
                case CreditOrganizationRequestStatus.Rejected:
                    return "<svg class=\"bi bi-x-circle-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" fill=\"#dc3545\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.146-3.146a.5.5 0 0 0-.708-.708L8 7.293 4.854 4.146a.5.5 0 1 0-.708.708L7.293 8l-3.147 3.146a.5.5 0 0 0 .708.708L8 8.707l3.146 3.147a.5.5 0 0 0 .708-.708L8.707 8l3.147-3.146z\"/></svg>";
                case CreditOrganizationRequestStatus.NotApplicable:
                    return "<svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-slash-circle-fill\" fill=\"#dc3545\" xmlns=\"http://www.w3.org/2000/svg\"><path fill-rule=\"evenodd\" d=\"M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.146-3.146a.5.5 0 0 0-.708-.708l-7 7a.5.5 0 0 0 .708.708l7-7z\"/></svg>";
                default:
                    return string.Empty;
            }
        }
    }
}