﻿﻿using System;

 namespace Admin.Web.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDateAndTimeFormat(this DateTime date)
        {
            return date.ToString("dd.MM.yyyy HH:mm");
        }
        
        public static string ToDateFormat(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("dd.MM.yyyy") : string.Empty;
        }
        
        public static string ToDateFormat(this DateTime date)
        {
            return date.ToString("dd.MM.yyyy");
        }
        
        public static string ToSqlFormat(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}