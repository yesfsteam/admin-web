﻿using System;
using Microsoft.Extensions.Configuration;

namespace Admin.Web.Extensions
{
    public static class ConfigurationExtensions
    {
        public static T BindFromAppConfig<T>(this IConfiguration configuration)
        {
            var type = typeof(T);
            var obj = (T)Activator.CreateInstance(type);
            configuration.Bind(type.Name, obj);
            return obj;
        }

    }
}