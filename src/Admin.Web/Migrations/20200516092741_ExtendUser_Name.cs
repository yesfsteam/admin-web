﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.Web.Migrations
{
    public partial class ExtendUser_Name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "asp_net_users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "name",
                table: "asp_net_users");
        }
    }
}
