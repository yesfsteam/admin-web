﻿namespace Admin.Web.Models
{
    public class Roles
    {
        public const string ADMIN = "Admin";
        public const string OPERATOR = "Operator";
    }
}