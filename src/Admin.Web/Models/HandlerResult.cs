﻿namespace Admin.Web.Models
{
    public class HandlerResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }
    }
}