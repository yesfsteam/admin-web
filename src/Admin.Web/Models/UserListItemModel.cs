﻿using System;

namespace Admin.Web.Models
{
    public class UserListItemModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsOperator { get; set; }
    }
}