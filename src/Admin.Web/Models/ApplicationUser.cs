﻿using Microsoft.AspNetCore.Identity;

namespace Admin.Web.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
    }
}