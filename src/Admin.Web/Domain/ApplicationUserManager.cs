﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Domain
{
    public interface IApplicationUserManager
    {
        List<UserListItemModel> GetUsers();
        Task<IdentityResult> CreateAsync(ApplicationUser user, string password);
        Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role);
        Task<IdentityResult> RemoveFromRoleAsync(ApplicationUser user, string role);
        Task<ApplicationUser> FindByIdAsync(string userId);
        Task<bool> IsInRoleAsync(ApplicationUser user, string role);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        //Task<IdentityResult> UpdatePasswordHash(ApplicationUser user, string newPassword, bool validatePassword);
        string GeneratePasswordHash(ApplicationUser user, string password);
    }
    
    public class ApplicationUserManager : UserManager<ApplicationUser>, IApplicationUserManager
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }
        
        public List<UserListItemModel> GetUsers()//todo
        {
            var users = Users.OrderBy(x => x.UserName).ToList();
            return users.Select(x => new UserListItemModel
            {
                UserId = Guid.Parse(x.Id),
                UserName = x.UserName,
                Name = x.Name,
                IsAdmin = IsInRoleAsync(x, Roles.ADMIN).GetSynchronousResult(),
                IsOperator = IsInRoleAsync(x, Roles.OPERATOR).GetSynchronousResult()
            }).ToList();
        }

        /*
        public new async Task<IdentityResult> UpdatePasswordHash(ApplicationUser user, string newPassword, bool validatePassword)
        {
            return await UpdatePasswordHash(user, newPassword, validatePassword);
        }
        */

        public string GeneratePasswordHash(ApplicationUser user, string password)
        {
            return PasswordHasher.HashPassword(user, password);
        }
    }
}