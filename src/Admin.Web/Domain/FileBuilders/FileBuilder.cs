﻿using System;
using System.Collections.Generic;
using System.IO;
using Yes.CreditApplication.Api.Contracts.Admin;

namespace Admin.Web.Domain.FileBuilders
{
    public interface IFileBuilder
    {
        MemoryStream BuildFile(FileType type, List<CreditApplicationListItemExportModel> items);
    }
    
    public class FileBuilder : IFileBuilder
    {
        public MemoryStream BuildFile(FileType type, List<CreditApplicationListItemExportModel> items)
        {
            switch (type)
            {
                case FileType.Csv:
                    return new CsvFileBuilder().BuildFile(items);
                case FileType.Excel:
                    return new ExcelFileBuilder().BuildFile(items);
                default:
                    throw new ArgumentException($"Invalid {nameof(type)} value", nameof(type));
            }
        }
    }
}