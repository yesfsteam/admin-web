﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Admin.Web.Extensions;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Domain.FileBuilders
{
    public class CsvFileBuilder
    {
        private const string YES = "Да";
        private const string NO = "Нет";
        
        public MemoryStream BuildFile(List<CreditApplicationListItemExportModel> items)
        {
            var ms = new MemoryStream();
            var builder = new StringBuilder();
            
            ms.Write(new byte[] {0xEF, 0xBB, 0xBF});  // BOM нужен, чтобы Excel нормально понял кодировку
            builder.Add("Идентификатор заявки");
            builder.Add("Тип анкеты");
            builder.Add("Дата заявки на кредит");
            builder.Add("Статус заявки");
            builder.Add("Шаг на лендинге");
            builder.Add("Название партнера");
            builder.Add("Фамилия клиента");
            builder.Add("Имя клиента");
            builder.Add("Отчество клиента");
            builder.Add("Номер телефона клиента");
            builder.Add("E-mail клиента");
            builder.Add("Срок кредита (в месяцах)");
            builder.Add("Сумма кредита");
            builder.Add("Дата согласия клиента на обработку персональных данных");
            builder.Add("Дата согласия клиента на отправку данных в БКИ");
            builder.Add("Дата рождения");
            builder.Add("Место рождения");
            builder.Add("Пол");
            builder.Add("Серия паспорта");
            builder.Add("Номер паспорта");
            builder.Add("Кем выдан");
            builder.Add("Дата выдачи паспорта");
            builder.Add("Код подразделения");
            builder.Add("Регион прописки");
            builder.Add("Код региона прописки по КЛАДР");
            builder.Add("Город прописки");
            builder.Add("Код города прописки по КЛАДР");
            builder.Add("Улица прописки");
            builder.Add("Код улицы прописки по КЛАДР");
            builder.Add("Дом прописки");
            builder.Add("Корпус прописки");
            builder.Add("Строение прописки");
            builder.Add("Квартира прописки");
            builder.Add("Код адреса прописки по КЛАДР");
            builder.Add("Регион проживания");
            builder.Add("Код региона проживания по КЛАДР");
            builder.Add("Город проживания");
            builder.Add("Код города проживания по КЛАДР");
            builder.Add("Улица проживания");
            builder.Add("Код улицы проживания по КЛАДР");
            builder.Add("Дом проживания");
            builder.Add("Корпус проживания");
            builder.Add("Строение проживания");
            builder.Add("Квартира проживания");
            builder.Add("Код адреса проживания по КЛАДР");
            builder.Add("Образование");
            builder.Add("Семейное положение");
            builder.Add("Количество детей (иждивенцев)");
            builder.Add("Подтверждающий документ");
            builder.Add("Наличие квартиры");
            builder.Add("Наличие дома");
            builder.Add("Наличие участка");
            builder.Add("Наличие автомобиля");
            builder.Add("Тип дополнительного телефона");
            builder.Add("Дополнительный телефон");
            builder.Add("Вид деятельности");
            builder.Add("ИНН");
            builder.Add("Доход в месяц");
            builder.Add("Наименование работодателя");
            builder.Add("Регион работодателя");
            builder.Add("Код региона работодателя по КЛАДР");
            builder.Add("Город работодателя");
            builder.Add("Код города работодателя по КЛАДР");
            builder.Add("Улица работодателя");
            builder.Add("Код улицы работодателя по КЛАДР");
            builder.Add("Дом работодателя");
            builder.Add("Корпус работодателя");
            builder.Add("Строение работодателя");
            builder.Add("Офис работодателя");
            builder.Add("Код адреса работодателя по КЛАДР");
            builder.Add("Отрасль работодателя");
            builder.Add("Численность работников");
            builder.Add("Рабочий телефон");
            builder.Add("Дата начала работы на текущем месте");
            builder.AppendLine("Должность");
            
            foreach (var item in items)
            {
                builder.Add(item.CreditApplicationId.ToString());
                builder.Add(item.ProfileType.GetDescription());
                builder.Add(item.Date);
                builder.Add(item.Status.GetDescription());
                builder.Add(item.Step.GetDescription());
                builder.Add(item.PartnerName);
                builder.Add(item.LastName);
                builder.Add(item.FirstName);
                builder.Add(item.MiddleName);
                builder.Add(item.PhoneNumber);
                builder.Add(item.Email);
                builder.Add(item.CreditPeriod.ToString());
                builder.Add(item.CreditAmount.ToString());
                builder.Add(item.PersonalDataProcessApproveDate);
                builder.Add(item.CreditBureauProcessApproveDate);
                builder.Add(item.DateOfBirth);
                builder.Add(item.PlaceOfBirth);
                builder.Add(item.Gender.GetDescription());
                builder.Add(item.PassportSeries);
                builder.Add(item.PassportNumber);
                builder.Add(item.PassportIssuer);
                builder.Add(item.PassportIssueDate);
                builder.Add(item.PassportDepartmentCode);
                builder.Add(item.RegistrationAddressRegion);
                builder.Add(item.RegistrationAddressRegionKladrCode);
                builder.Add(item.RegistrationAddressCity);
                builder.Add(item.RegistrationAddressCityKladrCode);
                builder.Add(item.RegistrationAddressStreet);
                builder.Add(item.RegistrationAddressStreetKladrCode);
                builder.Add(item.RegistrationAddressHouse);
                builder.Add(item.RegistrationAddressBlock);
                builder.Add(item.RegistrationAddressBuilding);
                builder.Add(item.RegistrationAddressApartment);
                builder.Add(item.RegistrationAddressKladrCode);
                builder.Add(item.ResidenceAddressRegion);
                builder.Add(item.ResidenceAddressRegionKladrCode);
                builder.Add(item.ResidenceAddressCity);
                builder.Add(item.ResidenceAddressCityKladrCode);
                builder.Add(item.ResidenceAddressStreet);
                builder.Add(item.ResidenceAddressStreetKladrCode);
                builder.Add(item.ResidenceAddressHouse);
                builder.Add(item.ResidenceAddressBlock);
                builder.Add(item.ResidenceAddressBuilding);
                builder.Add(item.ResidenceAddressApartment);
                builder.Add(item.ResidenceAddressKladrCode);
                builder.Add(item.Education.GetDescription());
                builder.Add(item.MaritalStatus.GetDescription());
                builder.Add(item.DependentsCount.ToString());
                builder.Add(item.ConfirmationDocument.GetDescription());
                builder.Add(item.HasFlat ? YES : NO);
                builder.Add(item.HasHouse ? YES : NO);
                builder.Add(item.HasArea ? YES : NO);
                builder.Add(item.HasCar ? YES : NO);
                builder.Add(item.AdditionalPhoneNumberType.GetDescription());
                builder.Add(item.AdditionalPhoneNumber);
                builder.Add(item.Activity.GetDescription());
                builder.Add(item.Tin);
                builder.Add(item.MonthlyIncome.HasValue ? item.MonthlyIncome.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
                builder.Add(item.EmployerName);
                builder.Add(item.EmployerAddressRegion);
                builder.Add(item.EmployerAddressRegionKladrCode);
                builder.Add(item.EmployerAddressCity);
                builder.Add(item.EmployerAddressCityKladrCode);
                builder.Add(item.EmployerAddressStreet);
                builder.Add(item.EmployerAddressStreetKladrCode);
                builder.Add(item.EmployerAddressHouse);
                builder.Add(item.EmployerAddressBlock);
                builder.Add(item.EmployerAddressBuilding);
                builder.Add(item.EmployerAddressApartment);
                builder.Add(item.EmployerAddressKladrCode);
                builder.Add(item.EmployerIndustry.GetDescription());
                builder.Add(item.EmployerStaff.HasValue ? item.EmployerStaff.Value.ToString() : string.Empty);
                builder.Add(item.EmployerPhoneNumber);
                builder.Add(item.EmployeeStartDate);
                builder.AppendLine(item.EmployeePosition.GetDescription());
            }
            
            ms.Write(Encoding.UTF8.GetBytes(builder.ToString()));
            ms.Seek(0, SeekOrigin.Begin);
            
            return ms;
        }
    }
    
    public static class StringBuilderExtension
    {
        private const string SEPARATOR = ";";
        
        public static void Add(this StringBuilder builder, string value)
        {
            builder.Append(value);
            builder.Append(SEPARATOR);
        }
        
        public static void Add(this StringBuilder builder, DateTime value)
        {
            builder.Append(value.ToDateFormat());
            builder.Append(SEPARATOR);
        }
        
        public static void Add(this StringBuilder builder, DateTime? value)
        {
            builder.Append(value.ToDateFormat());
            builder.Append(SEPARATOR);
        }
    }
}