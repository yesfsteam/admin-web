﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Admin.Web.Extensions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Domain.FileBuilders
{
    public class ExcelFileBuilder
    {
        private const string YES = "Да";
        private const string NO = "Нет";
        
        public MemoryStream BuildFile(List<CreditApplicationListItemExportModel> items)
        {
            var ms = new MemoryStream();
            using var document = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook);
            var workbookpart = document.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();
            
            var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
            var sheetData = new SheetData();
            worksheetPart.Worksheet = new Worksheet(sheetData);
            
            var sheets = document.WorkbookPart.Workbook.AppendChild(new Sheets());
            var sheet = new Sheet
            {
                Id = document.WorkbookPart.GetIdOfPart(worksheetPart),
                SheetId = 1,
                Name = "Заявки на кредит"
            };
            sheets.AppendChild(sheet);
            
            var headerRow = new Row { RowIndex = 1 };
            headerRow.AddCell("Идентификатор заявки");
            headerRow.AddCell("Тип анкеты");
            headerRow.AddCell("Дата заявки на кредит");
            headerRow.AddCell("Статус заявки");
            headerRow.AddCell("Шаг на лендинге");
            headerRow.AddCell("Название партнера");
            headerRow.AddCell("Фамилия клиента");
            headerRow.AddCell("Имя клиента");
            headerRow.AddCell("Отчество клиента");
            headerRow.AddCell("Номер телефона клиента");
            headerRow.AddCell("E-mail клиента");
            headerRow.AddCell("Срок кредита (в месяцах)");
            headerRow.AddCell("Сумма кредита");
            headerRow.AddCell("Дата согласия клиента на обработку персональных данных");
            headerRow.AddCell("Дата согласия клиента на отправку данных в БКИ");
            headerRow.AddCell("Дата рождения");
            headerRow.AddCell("Место рождения");
            headerRow.AddCell("Пол");
            headerRow.AddCell("Серия паспорта");
            headerRow.AddCell("Номер паспорта");
            headerRow.AddCell("Кем выдан");
            headerRow.AddCell("Дата выдачи паспорта");
            headerRow.AddCell("Код подразделения");
            headerRow.AddCell("Регион прописки");
            headerRow.AddCell("Код региона прописки по КЛАДР");
            headerRow.AddCell("Город прописки");
            headerRow.AddCell("Код города прописки по КЛАДР");
            headerRow.AddCell("Улица прописки");
            headerRow.AddCell("Код улицы прописки по КЛАДР");
            headerRow.AddCell("Дом прописки");
            headerRow.AddCell("Корпус прописки");
            headerRow.AddCell("Строение прописки");
            headerRow.AddCell("Квартира прописки");
            headerRow.AddCell("Код адреса прописки по КЛАДР");
            headerRow.AddCell("Регион проживания");
            headerRow.AddCell("Код региона проживания по КЛАДР");
            headerRow.AddCell("Город проживания");
            headerRow.AddCell("Код города проживания по КЛАДР");
            headerRow.AddCell("Улица проживания");
            headerRow.AddCell("Код улицы проживания по КЛАДР");
            headerRow.AddCell("Дом проживания");
            headerRow.AddCell("Корпус проживания");
            headerRow.AddCell("Строение проживания");
            headerRow.AddCell("Квартира проживания");
            headerRow.AddCell("Код адреса проживания по КЛАДР");
            headerRow.AddCell("Образование");
            headerRow.AddCell("Семейное положение");
            headerRow.AddCell("Количество детей (иждивенцев)");
            headerRow.AddCell("Подтверждающий документ");
            headerRow.AddCell("Наличие квартиры");
            headerRow.AddCell("Наличие дома");
            headerRow.AddCell("Наличие участка");
            headerRow.AddCell("Наличие автомобиля");
            headerRow.AddCell("Тип дополнительного телефона");
            headerRow.AddCell("Дополнительный телефон");
            headerRow.AddCell("Вид деятельности");
            headerRow.AddCell("ИНН");
            headerRow.AddCell("Доход в месяц");
            headerRow.AddCell("Наименование работодателя");
            headerRow.AddCell("Регион работодателя");
            headerRow.AddCell("Код региона работодателя по КЛАДР");
            headerRow.AddCell("Город работодателя");
            headerRow.AddCell("Код города работодателя по КЛАДР");
            headerRow.AddCell("Улица работодателя");
            headerRow.AddCell("Код улицы работодателя по КЛАДР");
            headerRow.AddCell("Дом работодателя");
            headerRow.AddCell("Корпус работодателя");
            headerRow.AddCell("Строение работодателя");
            headerRow.AddCell("Офис работодателя");
            headerRow.AddCell("Код адреса работодателя по КЛАДР");
            headerRow.AddCell("Отрасль работодателя");
            headerRow.AddCell("Численность работников");
            headerRow.AddCell("Рабочий телефон");
            headerRow.AddCell("Дата начала работы на текущем месте");
            headerRow.AddCell("Должность");
            sheetData.Append(headerRow);

            foreach (var item in items)
            {
                var dataRow = new Row();
                dataRow.AddCell(item.CreditApplicationId.ToString());
                dataRow.AddCell(item.ProfileType.GetDescription());
                dataRow.AddCell(item.Date);
                dataRow.AddCell(item.Status.GetDescription());
                dataRow.AddCell(item.Step.GetDescription());
                dataRow.AddCell(item.PartnerName);
                dataRow.AddCell(item.LastName);
                dataRow.AddCell(item.FirstName);
                dataRow.AddCell(item.MiddleName);
                dataRow.AddCell(item.PhoneNumber);
                dataRow.AddCell(item.Email);
                dataRow.AddCell(item.CreditPeriod.ToString());
                dataRow.AddCell(item.CreditAmount.ToString());
                dataRow.AddCell(item.PersonalDataProcessApproveDate);
                dataRow.AddCell(item.CreditBureauProcessApproveDate);
                dataRow.AddCell(item.DateOfBirth);
                dataRow.AddCell(item.PlaceOfBirth);
                dataRow.AddCell(item.Gender.GetDescription());
                dataRow.AddCell(item.PassportSeries);
                dataRow.AddCell(item.PassportNumber);
                dataRow.AddCell(item.PassportIssuer);
                dataRow.AddCell(item.PassportIssueDate);
                dataRow.AddCell(item.PassportDepartmentCode);
                dataRow.AddCell(item.RegistrationAddressRegion);
                dataRow.AddCell(item.RegistrationAddressRegionKladrCode);
                dataRow.AddCell(item.RegistrationAddressCity);
                dataRow.AddCell(item.RegistrationAddressCityKladrCode);
                dataRow.AddCell(item.RegistrationAddressStreet);
                dataRow.AddCell(item.RegistrationAddressStreetKladrCode);
                dataRow.AddCell(item.RegistrationAddressHouse);
                dataRow.AddCell(item.RegistrationAddressBlock);
                dataRow.AddCell(item.RegistrationAddressBuilding);
                dataRow.AddCell(item.RegistrationAddressApartment);
                dataRow.AddCell(item.RegistrationAddressKladrCode);
                dataRow.AddCell(item.ResidenceAddressRegion);
                dataRow.AddCell(item.ResidenceAddressRegionKladrCode);
                dataRow.AddCell(item.ResidenceAddressCity);
                dataRow.AddCell(item.ResidenceAddressCityKladrCode);
                dataRow.AddCell(item.ResidenceAddressStreet);
                dataRow.AddCell(item.ResidenceAddressStreetKladrCode);
                dataRow.AddCell(item.ResidenceAddressHouse);
                dataRow.AddCell(item.ResidenceAddressBlock);
                dataRow.AddCell(item.ResidenceAddressBuilding);
                dataRow.AddCell(item.ResidenceAddressApartment);
                dataRow.AddCell(item.ResidenceAddressKladrCode);
                dataRow.AddCell(item.Education.GetDescription());
                dataRow.AddCell(item.MaritalStatus.GetDescription());
                dataRow.AddCell(item.DependentsCount.ToString());
                dataRow.AddCell(item.ConfirmationDocument.GetDescription());
                dataRow.AddCell(item.HasFlat ? YES : NO);
                dataRow.AddCell(item.HasHouse ? YES : NO);
                dataRow.AddCell(item.HasArea ? YES : NO);
                dataRow.AddCell(item.HasCar ? YES : NO);
                dataRow.AddCell(item.AdditionalPhoneNumberType.GetDescription());
                dataRow.AddCell(item.AdditionalPhoneNumber);
                dataRow.AddCell(item.Activity.GetDescription());
                dataRow.AddCell(item.Tin);
                dataRow.AddCell(item.MonthlyIncome.HasValue ? item.MonthlyIncome.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
                dataRow.AddCell(item.EmployerName);
                dataRow.AddCell(item.EmployerAddressRegion);
                dataRow.AddCell(item.EmployerAddressRegionKladrCode);
                dataRow.AddCell(item.EmployerAddressCity);
                dataRow.AddCell(item.EmployerAddressCityKladrCode);
                dataRow.AddCell(item.EmployerAddressStreet);
                dataRow.AddCell(item.EmployerAddressStreetKladrCode);
                dataRow.AddCell(item.EmployerAddressHouse);
                dataRow.AddCell(item.EmployerAddressBlock);
                dataRow.AddCell(item.EmployerAddressBuilding);
                dataRow.AddCell(item.EmployerAddressApartment);
                dataRow.AddCell(item.EmployerAddressKladrCode);
                dataRow.AddCell(item.EmployerIndustry.GetDescription());
                dataRow.AddCell(item.EmployerStaff.HasValue ? item.EmployerStaff.Value.ToString() : string.Empty);
                dataRow.AddCell(item.EmployerPhoneNumber);
                dataRow.AddCell(item.EmployeeStartDate);
                dataRow.AddCell(item.EmployeePosition.GetDescription());
                sheetData.Append(dataRow);
            }

            workbookpart.Workbook.Save();
            document.Close();

            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }
    }

    public static class RowExtensions
    {
        public static void AddCell(this Row row, string cellValue)
        {
            row.AppendChild(new Cell { CellValue = new CellValue(cellValue), DataType = CellValues.String });
        }
        
        public static void AddCell(this Row row, DateTime cellValue)
        {
            row.AppendChild(new Cell { CellValue = new CellValue(cellValue.ToDateFormat()), DataType = CellValues.String });
        }
        
        public static void AddCell(this Row row, DateTime? cellValue)
        {
            row.AppendChild(new Cell { CellValue = new CellValue(cellValue.ToDateFormat()), DataType = CellValues.String });
        }
    }
}