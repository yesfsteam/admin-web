﻿namespace Admin.Web.Domain.FileBuilders
{
    public enum FileType
    {
        Csv = 1,
        Excel = 2
    }
}