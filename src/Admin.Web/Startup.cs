using System.Reflection;
using Admin.Web.Data;
using Admin.Web.Domain;
using Admin.Web.Domain.FileBuilders;
using Admin.Web.Extensions;
using Admin.Web.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Yes.CreditApplication.Api.Client;
using Yes.Infrastructure.Http.Extensions;

namespace Admin.Web
{
    public class Startup
    {
        private readonly string applicationName;
        
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        private readonly IConfiguration configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages()
                .AddRazorPagesOptions(options =>
                {
                    
                    //options.Conventions.AuthorizePage("/Index");
                    options.Conventions.AllowAnonymousToPage("/Login");
                    options.Conventions.AllowAnonymousToPage("/ForgotPassword");
                    options.Conventions.AllowAnonymousToPage("/ForgotPasswordConfirmation");
                    options.Conventions.AuthorizeFolder("/");
                    //options.Conventions.AddPageRoute("/Login", "login");
                    options.Conventions.AddPageRoute("/Users", "/users");
                    options.Conventions.AddPageRoute("/CreateUser", "users/create");
                    options.Conventions.AddPageRoute("/User", "/users/{userId:Guid}");
                    options.Conventions.AddPageRoute("/CreditApplication", "credit-applications/{creditApplicationId:Guid}");
                    options.Conventions.AddPageRoute("/CreditApplicationEdit", "credit-applications/{creditApplicationId:Guid}/edit");
                });
            services.AddRouting(options => options.LowercaseUrls = true);
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = new PathString("/Login");
            });
            
            //Configure antiforgery cookie
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddDbContext<DataProtectionContext>(options => options.UseNpgsql(configuration.GetConnectionString("Database")));
            services.AddDataProtection().PersistKeysToDbContext<DataProtectionContext>();
            
            services.AddHttpContextAccessor();
            
            services.AddSingleton(configuration.BindFromAppConfig<ApplicationConfiguration>());
            services.AddTransient<IApplicationUserManager, ApplicationUserManager>();
            services.AddTransient<IFileBuilder, FileBuilder>();
            services.AddHttpClientFromConfiguration<IAdminCreditApplicationClient, AdminCreditApplicationClient>(
                configuration, "CreditApplicationApi");
            
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            applicationLifetime.ApplicationStopping.Register(OnApplicationStopping);
            
            if (!env.IsProduction())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
            Log.Logger.Information($"{applicationName} has been started");
        }
        
        private void OnApplicationStopping()
        {
            Log.Logger.Information($"{applicationName} has been stopped");
        }
    }
}
