﻿using Admin.Web.Extensions;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Admin.Web.Data
{
    public class DataProtectionContext : DbContext, IDataProtectionKeyContext
    {
        public DataProtectionContext(DbContextOptions<DataProtectionContext> options) : base(options)
        {
            
        }

        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ConvertToSnakeCase();
        }
    }
}