using Admin.Web.Areas.Identity.Data;
using Admin.Web.Domain;
using Admin.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(Admin.Web.Areas.Identity.IdentityHostingStartup))]
namespace Admin.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<IdentityDataContext>(options =>
                    options.UseNpgsql(context.Configuration.GetConnectionString("Database")));

                services.AddDefaultIdentity<ApplicationUser>(options =>
                        {
                            options.SignIn.RequireConfirmedAccount = false;
                            options.Password.RequiredLength = 5;
                            options.Password.RequireNonAlphanumeric = false;
                            options.Password.RequireLowercase = false;
                            options.Password.RequireUppercase = false;
                            options.Password.RequireDigit = false;
                        }
                    )
                    .AddRoles<IdentityRole>()
                    .AddUserManager<ApplicationUserManager>()
                    .AddEntityFrameworkStores<IdentityDataContext>()
                    .AddClaimsPrincipalFactory<ApplicationUserClaimsPrincipalFactory>();  ;
            });
        }
    }
}