using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Admin.Web.Domain;
using Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class CreateUserModel : PageModel
    {
        private readonly ILogger<CreateUserModel> logger;
        private readonly IApplicationUserManager userManager;
        
        public CreateUserModel(ILogger<CreateUserModel> logger, IApplicationUserManager userManager)
        {
            this.logger = logger;
            this.userManager = userManager;
        }
        
        [BindProperty]
        public UserFormModel Input { get; set; }

        
        public class UserFormModel
        {
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "Имя пользователя")]
            public string UserName { get; set; }
                
            [Required]
            [StringLength(100, ErrorMessage = "Длинна пароля должна быть не менее {2} символов", MinimumLength = 5)]
            [DataType(DataType.Password)]
            [Display(Name = "Пароль")]
            public string Password { get; set; }
    
            [DataType(DataType.Password)]
            [Display(Name = "Подтверждение пароля")]
            [Compare("Password", ErrorMessage = "Значения паролей не совпадают")]
            public string ConfirmPassword { get; set; }
                
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "ФИО")]
            public string Name { get; set; }
                
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
                
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "Роль")]
            public string Role { get; set; }
        }
        
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = Input.UserName, 
                    Email = Input.Email,
                    Name = Input.Name
                };
                var result = await userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    logger.LogInformation($"User successfully created. User: {user.ToJsonString()}");
                    await userManager.AddToRoleAsync(user, Input.Role);

                    return RedirectToPage("/Users");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            return Page();
        }
    }
}
