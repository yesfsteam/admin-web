﻿using System;
using System.Collections.Generic;
using Admin.Web.Domain;
using Admin.Web.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class UsersModel : PageModel
    {
        private readonly ILogger<UsersModel> logger;
        private readonly IApplicationUserManager userManager;

        public UsersModel(ILogger<UsersModel> logger, IApplicationUserManager userManager)
        {
            this.logger = logger;
            this.userManager = userManager;
        }
        
        public List<UserListItemModel> Users { get; set; }
        
        public void OnGet()
        {
            var beginTime = DateTime.Now;
            try
            {
                Users = userManager.GetUsers();
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit applications. Duration: {beginTime.GetDuration()}");
            }
        }

    }
}
