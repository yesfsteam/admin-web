using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Admin.Web.Domain;
using Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class UserModel : PageModel
    {
        private readonly ILogger<CreateUserModel> logger;
        private readonly IApplicationUserManager userManager;
        
        public UserModel(ILogger<CreateUserModel> logger, IApplicationUserManager userManager)
        {
            this.logger = logger;
            this.userManager = userManager;
        }

        [BindProperty]
        public UserFormModel Input { get; set; }

        public class UserFormModel
        {
            public string UserId { get; set; }
            
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "Имя пользователя")]
            public string UserName { get; set; }
            
            [StringLength(100, ErrorMessage = "Длинна пароля должна быть не менее {2} символов", MinimumLength = 5)]
            [DataType(DataType.Password)]
            [Display(Name = "Пароль")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Подтверждение пароля")]
            [Compare("Password", ErrorMessage = "Значения паролей не совпадают")]
            public string ConfirmPassword { get; set; }
            
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "ФИО")]
            public string Name { get; set; }
            
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
            
            [Required(ErrorMessage = "Поле обязательно для заполнения")]
            [Display(Name = "Роль")]
            public string Role { get; set; }
        }
        
        public async Task OnGetAsync(Guid userId)
        {
            var user = await userManager.FindByIdAsync(userId.ToString());
            Input = new UserFormModel
            {
                UserId = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                Name = user.Name,
                Role = await userManager.IsInRoleAsync(user, Roles.ADMIN) ? Roles.ADMIN : Roles.OPERATOR
            };
        }
        
        public async Task<IActionResult> OnPostAsync()
        {
            var isSuccess = true;
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByIdAsync(Input.UserId);
                if (user.UserName != Input.UserName || user.Email != Input.Email || user.Name != Input.Name || !string.IsNullOrWhiteSpace(Input.Password))
                {
                    user.UserName = Input.UserName;
                    user.Email = Input.Email;
                    user.Name = Input.Name;
                    if (!string.IsNullOrWhiteSpace(Input.Password))
                        user.PasswordHash = userManager.GeneratePasswordHash(user, Input.Password);
                    
                    var result = await userManager.UpdateAsync(user);
                    if (result.Succeeded)
                        logger.LogInformation($"User successfully updated. User: {user.ToJsonString()}");
                    else
                    {
                        isSuccess = false;
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }    
                    }
                }
                /*
                if (!string.IsNullOrWhiteSpace(Input.Password))
                {
                    var result = await userManager.UpdatePasswordHash(user, Input.Password, false);
                    if (result.Succeeded)
                        logger.LogInformation($"User password successfully updated. User: {user.ToJsonString()}");
                    else
                    {
                        isSuccess = false;
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }*/

                if (!await userManager.IsInRoleAsync(user, Input.Role))
                {
                    var roleForRemove = Input.Role == Roles.ADMIN ? Roles.OPERATOR : Roles.ADMIN;
                    await userManager.RemoveFromRoleAsync(user, roleForRemove);
                    var result = await userManager.AddToRoleAsync(user, Input.Role);
                    if (result.Succeeded)
                        logger.LogInformation($"User role successfully changed from {roleForRemove} to {Input.Role}. User: {user.ToJsonString()}");
                    else
                    {
                        isSuccess = false;
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            else
            {
                isSuccess = false;
            }
            
            if (isSuccess)
                return RedirectToPage("/Users");
            return Page();
        }
    }
}
