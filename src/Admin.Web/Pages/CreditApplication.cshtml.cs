﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Admin.Web.Extensions;
using Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.CreditApplication.Api.Client;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class CreditApplication : PageModel
    {
        private readonly ILogger<CreditApplication> logger;
        private readonly IAdminCreditApplicationClient client;
        
        public CreditApplication(ILogger<CreditApplication> logger, IAdminCreditApplicationClient client)
        {
            this.logger = logger;
            this.client = client;
        }
        
        public string Name { get; set; }
        public Guid CreditApplicationId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int? CreditAmount { get; set; }
        public int? CreditPeriod { get; set; }
        public string CreditOrganizationName { get; set; }
        public ProfileType ProfileType { get; set; }
        public string CreditApplicationDate { get; set; }
        public CreditApplicationStatus Status { get; set; }
        public CreditApplicationStep Step { get; set; }
        public List<CreditApplicationDecisionModel> CreditApplicationDecisions { get; set; }
        public List<CreditApplicationChangeModel> CreditApplicationChanges { get; set; }
        
        public async Task<IActionResult> OnGetAsync(Guid creditApplicationId)
        {
            var response = await client.GetCreditApplicationViewModel(creditApplicationId);
            if (response.IsSuccessStatusCode)
            {
                var creditApplication = response.Content;
                Name = $"{creditApplication.LastName} {creditApplication.FirstName} {creditApplication.MiddleName}";
                CreditApplicationId = creditApplicationId;
                PhoneNumber = creditApplication.PhoneNumber;
                Email = creditApplication.Email;
                CreditAmount = creditApplication.CreditAmount;
                CreditPeriod = creditApplication.CreditPeriod;
                CreditOrganizationName = creditApplication.PartnerName;
                ProfileType = creditApplication.ProfileType;
                CreditApplicationDate = creditApplication.Date.ToDateAndTimeFormat();
                Status = creditApplication.Status;
                Step = creditApplication.Step;
                CreditApplicationDecisions = creditApplication.CreditApplicationDecisions;
                CreditApplicationChanges = creditApplication.CreditApplicationChanges;
            }
            return Page();
        }
        
        public async Task<PartialViewResult> OnGetCreditApplicationDecisions(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var response = await client.GetCreditApplicationDecisions(creditApplicationId);
                if (response.IsSuccessStatusCode)
                {
                    CreditApplicationDecisions = response.Content;
                    return Partial("_CreditApplicationDecisions", CreditApplicationDecisions);
                }
                logger.LogError($"Error while getting credit application decisions. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, ErrorMessage: {response.ErrorMessage}");
                return Partial("_Error");
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application decisions. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}");
                return Partial("_Error");
            }
        }
        
        public async Task<IActionResult> OnPostUpdateAsync(Guid creditApplicationId, [FromBody]ChangeCreditApplicationStatusModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var response = await client.ChangeCreditApplicationStatus(creditApplicationId, model);
                if (response.IsSuccessStatusCode)
                {
                    return new JsonResult(new HandlerResult
                    {
                        IsSuccess = true,
                        RedirectUrl = model.Status == CreditApplicationStatus.InProgress
                            ? Url.Page("/CreditApplication", new {creditApplicationId})
                            : Url.Page("/Index")
                    });
                }
                logger.LogError($"Error while changing credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}, ErrorMessage: {response.ErrorMessage}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при смене статуса заявки"
                });
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while changing credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {model}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при смене статуса заявки"
                });
            }
        }
        
        public async Task<IActionResult> OnGetConfirmCreditApplicationAsync(Guid creditApplicationId, Guid creditOrganizationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var response = await client.ConfirmCreditApplication(creditApplicationId, new CreditApplicationRequest
                {
                    CreditOrganizationId = creditOrganizationId
                });
                if (response.IsSuccessStatusCode)
                    return new JsonResult(new HandlerResult {IsSuccess = true});
                
                logger.LogError($"Error while confirming credit application. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, ErrorMessage: {response.ErrorMessage}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при отправке запроса"
                });
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while confirming credit application. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при отправке запроса"
                });
            }
        }
        
        public async Task<IActionResult> OnGetResendCreditApplicationRequestAsync(Guid creditApplicationId, Guid creditOrganizationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var response = await client.ResendCreditApplicationRequest(creditApplicationId, new CreditApplicationRequest
                {
                    CreditOrganizationId = creditOrganizationId
                });
                if (response.IsSuccessStatusCode)
                    return new JsonResult(new HandlerResult {IsSuccess = true});
                
                logger.LogError($"Error while getting credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}, ErrorMessage: {response.ErrorMessage}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при отправке запроса"
                });
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit application status. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, CreditApplicationId: {creditApplicationId}, CreditOrganizationId: {creditOrganizationId}");
                return new JsonResult(new HandlerResult
                {
                    IsSuccess = false,
                    Message = "Ошибка при отправке запроса"
                });
            }
        }
    }
}
