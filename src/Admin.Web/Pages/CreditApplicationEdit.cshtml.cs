﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.CreditApplication.Api.Client;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class CreditApplicationEdit : PageModel
    {
        private readonly ILogger<CreditApplicationEdit> logger;
        private readonly IAdminCreditApplicationClient client;
        
        public CreditApplicationEdit(ILogger<CreditApplicationEdit> logger, IAdminCreditApplicationClient client)
        {
            this.logger = logger;
            this.client = client;
        }
        
        [BindProperty]
        public CreditApplicationEditViewModel CreditApplication { get; set; }
        
        public async Task<IActionResult> OnGetAsync(Guid creditApplicationId)
        {
            var response = await client.GetCreditApplicationEditViewModel(creditApplicationId);
            if (response.IsSuccessStatusCode)
            {
                if (response.Content.Status != CreditApplicationStatus.InProgress)
                {
                    return new RedirectToPageResult("/CreditApplication", new {creditApplicationId});
                }
                CreditApplication = response.Content;
            }
            return Page();
        }
        
        public async Task<IActionResult> OnPostAsync(Guid creditApplicationId)
        {
            var beginTime = DateTime.Now;
            try
            {
                var response = await client.UpdateCreditApplication(creditApplicationId, CreditApplication);
                if (!response.IsSuccessStatusCode)
                    return Page();
                return new RedirectToPageResult("/CreditApplication", new {creditApplicationId});
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while updating credit application. Duration: {beginTime.GetDuration()} CreditApplicationId: {creditApplicationId}, Request: {CreditApplication}");
                return Page();
            }
        }
    }
}
