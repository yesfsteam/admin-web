﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Admin.Web.Domain.FileBuilders;
using Admin.Web.Extensions;
using Admin.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Yes.CreditApplication.Api.Client;
using Yes.CreditApplication.Api.Contracts.Admin;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.Infrastructure.Common.Extensions;

namespace Admin.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> logger;
        private readonly IAdminCreditApplicationClient client;
        private readonly IFileBuilder fileBuilder;
        
        public IndexModel(ILogger<IndexModel> logger, IAdminCreditApplicationClient client, IFileBuilder fileBuilder, ApplicationConfiguration configuration)
        {
            this.logger = logger;
            this.client = client;
            this.fileBuilder = fileBuilder;
            PageSize = configuration.PageSize;
        }
        
        [BindProperty(SupportsGet = true)]
        public int PageNumber { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string DateFrom { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string DateTo { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string DateOfBirth { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string PhoneNumber { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string LastName { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string FirstName { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public string MiddleName { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public List<int> Statuses { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public List<int> ProfileTypes { get; set; }
        
        public long Total { get; set; }
        public int PageSize { get; }
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(Total, PageSize));
        public bool PreviousButtonEnabled => PageNumber > 1;
        public bool NextButtonEnabled => PageNumber < TotalPages;
        
        public List<CreditApplicationListItemModel> CreditApplications { get; set; }
        
        public async Task OnGetAsync()
        {
            DateFrom = DateTime.Today.ToDateFormat();
            await OnGetSearchAsync();
        }

        public async Task OnGetSearchAsync()
        {
            var beginTime = DateTime.Now;
            try
            {
                if (PageNumber <= 0)
                    PageNumber = 1;
                if (Statuses.Contains(0))
                    Statuses = new List<int>();
                if (ProfileTypes.Contains(0))
                    ProfileTypes = new List<int>();
                var request = new CreditApplicationListFilterModel
                {
                    Take = PageSize,
                    Skip = PageSize * (PageNumber - 1),
                    DateFrom = getDate(DateFrom),
                    DateTo = getDate(DateTo),
                    DateOfBirth = getDate(DateOfBirth),
                    Statuses = Statuses.Select(x => (CreditApplicationStatus)x).ToList(),
                    ProfileTypes = ProfileTypes.Select(x => (ProfileType)x).ToList(),
                    LastName = LastName,
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    PhoneNumber = PhoneNumber
                };
                var response = await client.GetCreditApplications(request);
                if (response.IsSuccessStatusCode)
                {
                    CreditApplications = response.Content.Items;
                    Total = response.Content.Total;    
                }
                else
                {
                    logger.LogError($"Error while getting credit applications. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");   
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while getting credit applications. Duration: {beginTime.GetDuration()}");
            }
        }

        public async Task<IActionResult> OnGetExportCsvAsync()
        {
            return await onGetExportAsync(FileType.Csv);
        }

        public async Task<IActionResult> OnGetExportExcelAsync()
        {
            return await onGetExportAsync(FileType.Excel);
        }

        private async Task<IActionResult> onGetExportAsync(FileType fileType)
        {
            var beginTime = DateTime.Now;
            try
            {
                if (Statuses.Contains(0))
                    Statuses = new List<int>();
                if (ProfileTypes.Contains(0))
                    ProfileTypes = new List<int>();
                var request = new CreditApplicationListFilterModel
                {
                    DateFrom = getDate(DateFrom),
                    DateTo = getDate(DateTo),
                    DateOfBirth = getDate(DateOfBirth),
                    Statuses = Statuses.Select(x => (CreditApplicationStatus)x).ToList(),
                    ProfileTypes = ProfileTypes.Select(x => (ProfileType)x).ToList(),
                    LastName = LastName,
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    PhoneNumber = PhoneNumber
                };
                var response = await client.GetCreditApplicationsForExport(request);
                if (response.IsSuccessStatusCode)
                {
                    var fileStream = fileBuilder.BuildFile(fileType, response.Content);
                    return fileType == FileType.Csv
                        ? File(fileStream, "text/csv", $"CreditApplications-{DateTime.Now.ToSqlFormat()}.csv")
                        : File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $"CreditApplications-{DateTime.Now.ToSqlFormat()}.xlsx");
                }

                logger.LogError($"Error while getting credit applications for export. Duration: {beginTime.GetDuration()} Request: {request}, ErrorMessage: {response.ErrorMessage}");
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while creating credit applications excel file. Duration: {beginTime.GetDuration()}");
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        private DateTime? getDate(string date)
        {
            return string.IsNullOrWhiteSpace(date)
                ? default(DateTime?)
                : DateTime.ParseExact(date,"dd.MM.yyyy", CultureInfo.InvariantCulture);
        }
    }
}
